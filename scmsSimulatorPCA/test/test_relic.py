# -*- coding: utf-8 -*-
import unittest
from pyrelic.relic_wrapper import RelicUtil
from pyrelic.relic_wrapper import Hash
from pyrelic.relic_wrapper import BigNumber
from pyrelic.relic_wrapper import EllipticCurveConfiguration as ECConfig
from pyrelic.relic_wrapper import Point
from pyrelic.relic_wrapper import CryptoProtocols
import os


class TestRelicConfiguration(unittest.TestCase):

    def test_core_init(self):
        self.assertTrue(RelicUtil.core_init())


class TestHash(unittest.TestCase):
    def setUp(self):
        RelicUtil.core_init()

    def test_hash_sha256(self):
        input_text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum dapibus metus nec diam pretium cursus. Nam iaculis tortor a interdum imperdiet. Aliquam facilisis nulla mauris, in vestibulum diam placerat tincidunt. Cras quis metus a nulla congue ornare. Pellentesque at ornare felis. Phasellus ultrices, sapien nec convallis finibus, lectus sapien tempor eros, et ultricies dolor felis ac diam. Integer gravida et arcu et scelerisque. Nullam porttitor placerat tortor nec imperdiet."
        precalculated_hash_value = "6866982eafb76a6eb24d265a55c51bc05a174ef0a9bab55678fbaf439cf928a3"
        calculated_hash = Hash.hash_sha256(input_text)
        self.assertEqual(precalculated_hash_value, calculated_hash)


class TestBigNumber(unittest.TestCase):
    def setUp(self):
        RelicUtil.core_init()

    def test_assign_by_bin_value(self):
        big = BigNumber()
        printed_value = "390E4E179E904A11E76E8EE167C75A6F1AB651D551772167F8A649D79CF7B79A"
        bin_value = [57, 14, 78, 23, 158, 144, 74, 17, 231, 110,
                 142, 225, 103, 199, 90, 111, 26, 182, 81, 213,
                 81, 119, 33, 103, 248, 166, 73, 215, 156, 247, 183, 154]

        big.read_bin(bin_value)
        self.assertEqual(printed_value, str(big))

    def test_assign_by_bin_value_of_rand(self):
        big_rand = BigNumber()
        big = BigNumber()
        big_rand.rand()
        bin_value = big_rand.write_bin()
        big.read_bin(bin_value)
        self.assertEqual(big, big_rand)

    def test_assign_by_raw_values(self):
        big_rand = BigNumber()
        big_rand.rand()

        big_copy = BigNumber()
        big_copy.alloc = big_rand.alloc
        big_copy.used = big_rand.used
        big_copy.sign = big_rand.sign
        big_copy.dp = big_rand.dp

        self.assertEqual(big_rand, big_copy)

    def test_is_zero(self):
        big = BigNumber()
        self.assertTrue(big.is_zero())

    def test_copy(self):
        big = BigNumber()
        big.rand()

        big_copy = BigNumber()
        big.copy(big_copy)

        self.assertEqual(big, big_copy)

    def test_add(self):
        bin_1 = [1, 52, 235, 153, 69, 75, 11, 171, 83, 47, 92, 47, 117, 31,
                180, 70, 111, 17, 103, 133, 60, 229, 157, 25, 128, 55, 187,
                185, 125, 96, 243, 172]
        bin_2 = [12, 121, 109, 128, 47, 249, 174, 120, 241, 54, 175, 49, 119,
                158, 242, 140, 97, 100, 98, 203, 128, 206, 134, 178, 140, 59,
                244, 255, 233, 231, 93, 253]

        big_1 = BigNumber()
        big_2 = BigNumber()
        result = BigNumber()

        big_1.read_bin(bin_1)
        big_2.read_bin(bin_2)

        result_bin = [13, 174, 89, 25, 117, 68, 186, 36, 68, 102, 11, 96, 236,
                190, 166, 210, 208, 117, 202, 80, 189, 180, 35, 204, 12,
                115, 176, 185, 103, 72, 81, 169]

        BigNumber.add(result, big_1, big_2)
        self.assertEqual(result.write_bin(), result_bin)


class TestEllipticCurveConfiguration(unittest.TestCase):
    def setUp(self):
        RelicUtil.core_init()

    def test_param_set_any(self):
        self.assertTrue(ECConfig.param_set_any())

    def test_param_security_level(self):
        # Curva de segurança de 128 bits
        ECConfig.param_set_any()
        self.assertEqual(ECConfig.param_security_level(), 128)


class TestPoint(unittest.TestCase):
    def setUp(self):
        RelicUtil.core_init()
        ECConfig.param_set_any()

    def test_is_valid(self):
        pointA = Point()
        self.assertTrue(pointA.is_valid())

    def test_assign_by_bin_value(self):
        pointB = Point()
        bin_value = [4, 80, 28, 117, 63, 76, 101, 134, 155, 64, 69, 57, 65, 113, 252, 22, 111, 195, 110, 239, 237, 71, 230, 109, 85, 187, 142, 253, 86, 82, 162, 255, 64, 123, 84, 244, 180, 130, 98, 41, 207, 62, 250, 102, 139, 41, 101, 41, 117, 99, 234, 237, 121, 156, 3, 101, 111, 230, 226, 75, 204, 22, 77, 89, 104]
        expected_str = """501C753F4C65869B 4045394171FC166F C36EEFED47E66D55 BB8EFD5652A2FF40
7B54F4B4826229CF 3EFA668B29652975 63EAED799C03656F E6E24BCC164D5968
0000000000000000 0000000000000000 0000000000000000 0000000000000001"""
        pointB.read_bin(bin_value)
        self.assertEqual(str(pointB), expected_str)

    def test_assign_by_bin_value_of_rand(self):
        pointA = Point()
        pointB = Point()
        pointA.rand()
        bin_value = pointA.write_bin()
        pointB.read_bin(bin_value)
        self.assertEqual(pointA, pointB)

    def test_normalize(self):
        pointA = Point()
        normalized_point = pointA.normalize_point()

        self.assertEqual(pointA.norm, 0)
        self.assertEqual(normalized_point.norm, 1)

    def test_curve_order(self):
        curve_order = "1000000000000000000000000000000014DEF9DEA2F79CD65812631A5CF5D3ED"
        bn_curve_order = BigNumber()
        Point.get_curve_order(bn_curve_order)
        self.assertEqual(str(bn_curve_order), curve_order)

    def test_add(self):
        bin_a = [4, 19, 236, 131, 216, 75, 13, 132, 240, 174, 131, 230, 44, 135, 239, 183, 212, 30, 78, 151, 85, 190, 20, 113, 174, 119, 64, 205, 15, 227, 54, 188, 195, 77, 9, 96, 138, 147, 227, 76, 227, 109, 252, 108, 174, 125, 113, 155, 110, 222, 25, 221, 101, 11, 4, 191, 162, 124, 67, 50, 148, 104, 152, 203, 109]
        bin_b = [4, 64, 98, 3, 15, 98, 82, 189, 107, 27, 183, 132, 121, 210, 254, 157, 84, 218, 116, 26, 156, 222, 77, 250, 46, 249, 79, 39, 181, 136, 76, 205, 25, 20, 237, 175, 3, 211, 96, 22, 195, 13, 5, 229, 154, 198, 91, 169, 157, 130, 118, 223, 56, 38, 74, 238, 114, 213, 7, 124, 42, 41, 187, 251, 229]
        pointA = Point()
        pointB = Point()
        pointA.read_bin(bin_a)
        pointB.read_bin(bin_b)

        pointC = Point()
        pointC_str = """068DA7887090FEC3 C512FCDBCD98129A B95A2017B4502BDE A90E44CEF13BAE98
48B115D24125B61F 3CD230890AF3CE91 6476D2A5A7AA35FA 1D647201B92F6764
0000000000000000 0000000000000000 0000000000000000 0000000000000001"""

        Point.add(pointC, pointA, pointB)
        pointC_norm = pointC.normalize_point()
        self.assertEqual(str(pointC_norm), pointC_str)

    def test_multiply(self):
        bin_bn_a = [5, 165, 180, 245, 178, 175, 118, 179, 216, 111, 232, 113, 216, 121, 22, 243, 11, 103, 185, 111, 149, 58, 248, 163, 137, 57, 193, 133, 0, 189, 206, 144]
        bin_ec_b = [4, 33, 107, 186, 26, 46, 149, 112, 22, 136, 68, 144, 141, 7, 51, 84, 228, 97, 47, 80, 152, 104, 128, 45, 92, 2, 125, 228, 230, 36, 182, 48, 231, 74, 73, 192, 87, 18, 163, 26, 71, 166, 204, 72, 97, 245, 129, 127, 99, 238, 81, 64, 119, 32, 83, 36, 14, 125, 89, 148, 214, 54, 35, 59, 102]
        pointC_str = """5E87E8394DD85467 224692F7F90F10AF C225888D33EB1747 244127FD2D0B2761
04B12A901F2AEDF3 E62837CD2BDA7C5E F9FE1EE8649EF85A EEECB2BF35534EBE
0000000000000000 0000000000000000 0000000000000000 0000000000000001"""

        bigA = BigNumber()
        bigA.read_bin(bin_bn_a)

        pointB = Point()
        pointB.read_bin(bin_ec_b)

        pointC = Point()
        Point.multiply(pointC, pointB, bigA)
        pointC_norm = pointC.normalize_point()

        self.assertEqual(str(pointC_norm), pointC_str)

    def test_multiply_by_generator(self):
        bin_bn = [137, 122, 234, 7, 182, 46, 9, 194, 175, 40, 192, 123, 61, 83, 1, 115, 41, 54, 162, 90, 162, 142, 82, 214, 255, 141, 148, 147, 99, 10, 28]
        point_str = """0E2546B3864864D1 89D91CB83E7F2FCE 82C24D799251FE31 459E2CFF87523D05
78272A6D4090980F 46984F523F6FC969 51F49E3FDBE3DFDD 6548F17CF3408869
0000000000000000 0000000000000000 0000000000000000 0000000000000001"""

        bigA = BigNumber()
        bigA.read_bin(bin_bn)

        point = Point()

        Point.multiply_by_generator(point, bigA)
        point_norm = point.normalize_point()
        self.assertEqual(point_str, str(point_norm))

    def test_multiply_simul(self):
        bin_b = [3, 254, 192, 78, 10, 24, 155, 227, 181, 108, 21, 22, 205, 134, 20, 55, 152, 94, 174, 143, 104, 221, 227, 193, 85, 62, 203, 169, 65, 223, 164, 173]
        bin_d = [2, 100, 150, 70, 37, 3, 15, 219, 75, 54, 240, 72, 37, 180, 169, 51, 140, 70, 143, 140, 123, 179, 101, 43, 78, 107, 46, 41, 163, 121, 85, 229]
        bin_ec_f = [4, 3, 164, 18, 108, 62, 128, 173, 81, 54, 175, 237, 38, 201, 132, 166, 82, 112, 189, 178, 77, 197, 157, 168, 43, 101, 185, 86, 34, 48, 132, 2, 153, 115, 199, 114, 28, 103, 61, 208, 85, 11, 218, 149, 128, 222, 51, 243, 246, 15, 181, 146, 7, 95, 172, 71, 145, 146, 58, 165, 198, 226, 241, 200, 254]
        bin_ec_h = [4, 58, 104, 101, 65, 66, 150, 138, 79, 221, 73, 234, 74, 49, 33, 137, 12, 181, 201, 219, 149, 147, 5, 209, 60, 64, 252, 81, 87, 45, 183, 136, 96, 72, 23, 192, 83, 8, 195, 82, 100, 174, 171, 94, 241, 124, 166, 233, 91, 99, 16, 227, 81, 188, 12, 153, 163, 231, 215, 50, 5, 172, 69, 120, 26]

        point_str = """1C95A2916C0A446D EA28355377846294 52665FC559368DAD 00D24C0B156AE509
6EEC56795DF52A33 7BA7C4492EE33AE7 696DCEBC16147722 0311310BD1DB09EA
0000000000000000 0000000000000000 0000000000000000 0000000000000001"""

        bigB = BigNumber()
        bigB.read_bin(bin_b)

        bigD = BigNumber()
        bigD.read_bin(bin_d)

        pointF = Point()
        pointF.read_bin(bin_ec_f)

        pointH = Point()
        pointH.read_bin(bin_ec_h)

        pointR = Point()
        Point.multiply_simul(pointR, pointF, bigB, pointH, bigD)

        point_norm = pointR.normalize_point()
        self.assertEqual(point_str, str(point_norm))

    def test_multiply_by_generator_simul(self):
        bin_b = [12, 146, 150, 116, 85, 125, 53, 162, 71, 162, 54, 131, 56, 223, 179, 168, 17, 154, 227, 218, 182, 78, 144, 240, 10, 225, 14, 253, 152, 53, 72, 192]
        bin_d = [7, 124, 172, 5, 98, 164, 40, 78, 224, 56, 153, 152, 164, 41, 107, 144, 39, 162, 164, 206, 128, 139, 235, 222, 52, 109, 90, 184, 53, 64, 94, 174]
        bin_ec_f = [4, 4, 72, 31, 105, 21, 47, 142, 101, 246, 94, 54, 136, 182, 123, 249, 116, 64, 244, 95, 204, 123, 163, 205, 160, 108, 227, 5, 150, 77, 204, 36, 128, 0, 142, 94, 89, 178, 25, 118, 197, 128, 215, 35, 228, 168, 150, 125, 124, 169, 253, 60, 39, 167, 136, 253, 2, 120, 142, 125, 45, 48, 44, 113, 176]

        point_str = """4C9531D1A6F9655A 305B6ED6D4FDB247 65DD51F1ACFDC63A 1DDC23160825B05E
53B94C20DAFC377F 17654ED6309FF027 3E69D84BFF952816 5D0EAE120C89831F
0000000000000000 0000000000000000 0000000000000000 0000000000000001"""

        bigB = BigNumber()
        bigB.read_bin(bin_b)

        bigD = BigNumber()
        bigD.read_bin(bin_d)

        pointF = Point()
        pointF.read_bin(bin_ec_f)

        pointR = Point()
        Point.multiply_by_generator_simul(pointR, bigB, pointF, bigD)

        point_norm = pointR.normalize_point()
        self.assertEqual(point_str, str(point_norm))


class TestCryptoProtocols(unittest.TestCase):
    def setUp(self):
        RelicUtil.core_init()
        ECConfig.param_set_any()

    def test_generate_keys(self):
        private = BigNumber()
        public = Point()

        CryptoProtocols.generate_keys(private, public)

        public_manually_calculated = Point()
        Point.multiply_by_generator(public_manually_calculated, private)

        self.assertEqual(public, public_manually_calculated)

    def test_ecies(self):
        private = BigNumber()
        public = Point()
        CryptoProtocols.generate_keys(private, public)

        message = [253, 157, 48, 29, 170, 38, 56, 230, 16, 198, 64, 10, 12, 13, 28, 140, 244, 53, 93, 241, 102, 37, 200, 37, 221, 45, 70, 113, 85, 115, 108, 28]

        lista, pontoResultante = CryptoProtocols.ecies_encrypt(public, message)

        original_msg = CryptoProtocols.ecies_decrypt(pontoResultante, lista, private)
        self.assertEqual(original_msg, message)

    def test_ecdsa_sign(self):
        private = BigNumber()
        public = Point()
        CryptoProtocols.generate_keys(private, public)

        message = [4, 40, 95, 29, 144, 147, 190, 9, 243, 112, 64, 143, 174, 129, 141, 26, 245, 87, 182, 189, 211, 124, 45, 96, 245, 72, 140, 108, 133, 211, 164, 186, 100, 94, 215, 167, 142, 62, 171, 88, 217, 195, 67, 52, 173, 39, 236, 137, 179, 183, 115, 12, 202, 88, 81, 89, 213, 73, 37, 88, 156, 214, 171, 229, 166]

        r, s = CryptoProtocols.ecdsa_sign(message, private)

        self.assertTrue(CryptoProtocols.ecdsa_ver(message, r, s, public))

    def test_aes_cbc_encrypt(self):
        message = """CBC has been the most commonly used mode of operation. Its main drawbacks are that encryption is sequential (i.e., it cannot be parallelized), and that the message must be padded to a multiple of the cipher block size. One way to handle this last issue is through the method known as ciphertext stealing. Note that a one-bit change in a plaintext or initialization vector (IV) affects all following ciphertext blocks.
        Decrypting with the incorrect IV causes the first block of plaintext to be corrupt but subsequent plaintext blocks will be correct. This is because each block is XORed with the ciphertext of the previous block, not the plaintext, so one does not need to decrypt the previous block before using it as the IV for the decryption of the current one. This means that a plaintext block can be recovered from two adjacent blocks of ciphertext."""

        key = bytearray(b'\x9f\x80zI\xbc6\x97\x18\x9e\xf2\xdd\x06\x97N\xc0\xdd')
        iv = bytearray(b'\xc5\xba\x8d\xbf.\xef/>\xa1le}\xa2\xec\xa5\xb7')

        expected_encrypted = [65, 41, 148, 201, 53, 1, 39, 10, 17, 138, 192, 250, 0, 10, 192, 91, 238, 149, 239, 179, 168, 141, 36, 242, 116, 85, 194, 148, 184, 78, 225, 79, 128, 75, 200, 184, 191, 44, 56, 63, 179, 214, 247, 114, 26, 157, 251, 164, 89, 76, 194, 98, 74, 78, 37, 138, 59, 181, 0, 11, 76, 180, 12, 55, 69, 98, 1, 181, 187, 48, 70, 228, 93, 132, 81, 209, 3, 222, 240, 246, 53, 239, 247, 95, 98, 154, 193, 65, 202, 255, 32, 144, 101, 24, 227, 56, 139, 13, 42, 45, 22, 135, 118, 40, 86, 85, 39, 222, 18, 29, 111, 121, 102, 173, 249, 14, 22, 186, 249, 70, 90, 81, 129, 50, 135, 38, 161, 56, 135, 201, 26, 121, 167, 58, 21, 60, 176, 33, 197, 104, 54, 214, 72, 179, 36, 198, 20, 38, 109, 251, 197, 244, 139, 160, 22, 89, 5, 132, 229, 47, 56, 10, 158, 149, 57, 58, 37, 152, 179, 193, 103, 130, 189, 228, 174, 58, 230, 169, 250, 42, 63, 152, 105, 15, 120, 47, 91, 117, 160, 93, 241, 103, 125, 95, 238, 87, 154, 195, 175, 73, 157, 210, 196, 98, 17, 246, 212, 69, 168, 230, 248, 85, 154, 165, 78, 234, 47, 127, 97, 72, 232, 225, 133, 206, 210, 102, 169, 184, 162, 173, 182, 232, 201, 97, 104, 23, 149, 107, 164, 84, 225, 195, 84, 154, 21, 251, 196, 161, 137, 178, 67, 194, 217, 245, 188, 224, 109, 211, 208, 49, 169, 126, 40, 238, 91, 164, 126, 27, 33, 235, 30, 66, 106, 210, 45, 33, 33, 18, 183, 93, 188, 80, 233, 225, 56, 129, 201, 142, 84, 136, 208, 22, 77, 39, 3, 43, 97, 210, 112, 245, 109, 195, 255, 59, 210, 80, 88, 176, 64, 136, 131, 54, 58, 184, 204, 43, 161, 181, 77, 150, 189, 149, 19, 201, 224, 111, 116, 163, 84, 170, 197, 196, 152, 68, 124, 111, 41, 25, 35, 96, 55, 105, 147, 226, 27, 1, 44, 46, 61, 70, 219, 92, 126, 220, 172, 17, 106, 42, 87, 185, 240, 47, 250, 207, 62, 56, 206, 111, 169, 25, 120, 209, 159, 203, 34, 34, 112, 240, 23, 184, 213, 197, 128, 41, 20, 230, 252, 5, 34, 38, 76, 243, 68, 230, 51, 184, 95, 147, 195, 178, 122, 100, 254, 73, 189, 1, 243, 80, 134, 59, 4, 253, 169, 34, 157, 139, 136, 215, 18, 168, 15, 63, 78, 172, 34, 16, 48, 177, 239, 146, 214, 169, 151, 178, 159, 222, 121, 71, 26, 108, 251, 110, 100, 131, 6, 178, 240, 154, 82, 121, 129, 213, 84, 236, 8, 51, 47, 90, 15, 227, 93, 67, 74, 124, 204, 20, 69, 25, 193, 121, 53, 44, 123, 3, 122, 189, 64, 183, 228, 169, 62, 161, 226, 75, 186, 80, 160, 102, 224, 78, 173, 61, 166, 62, 97, 103, 207, 218, 77, 141, 254, 64, 243, 94, 16, 165, 199, 66, 177, 9, 128, 169, 86, 0, 52, 98, 7, 153, 116, 192, 167, 15, 158, 185, 155, 226, 60, 138, 48, 152, 209, 169, 19, 25, 106, 88, 13, 225, 110, 2, 65, 74, 115, 74, 67, 159, 208, 83, 75, 52, 214, 76, 24, 29, 244, 141, 176, 164, 72, 169, 141, 7, 210, 221, 33, 74, 78, 175, 238, 40, 17, 147, 78, 23, 61, 156, 128, 182, 95, 128, 27, 215, 131, 235, 245, 38, 10, 80, 155, 213, 58, 118, 221, 14, 34, 200, 14, 146, 116, 10, 233, 190, 207, 126, 131, 59, 171, 110, 28, 17, 36, 166, 194, 30, 65, 188, 111, 138, 14, 158, 29, 67, 203, 64, 96, 35, 53, 152, 1, 77, 122, 161, 24, 215, 82, 227, 255, 25, 68, 252, 127, 158, 41, 230, 85, 76, 46, 39, 194, 71, 158, 140, 245, 132, 210, 91, 102, 34, 212, 62, 179, 59, 218, 242, 86, 145, 101, 58, 211, 27, 12, 171, 147, 48, 127, 154, 81, 187, 174, 242, 45, 200, 11, 96, 174, 56, 176, 64, 120, 180, 59, 171, 75, 36, 224, 171, 208, 150, 211, 253, 35, 123, 134, 184, 131, 139, 23, 61, 41, 77, 251, 245, 123, 251, 8, 167, 61, 150, 142, 123, 239, 31, 54, 188, 229, 91, 2, 153, 37, 86, 222, 49, 167, 113, 4, 174, 168, 48, 69, 158, 197, 9, 129, 136, 100, 34, 241, 216, 198, 16, 144, 2, 113, 181, 202, 27, 156, 68, 30, 25, 228, 72, 247, 19, 75, 107, 136, 60, 75, 248, 59, 187, 134, 35, 50, 134, 177, 88, 16, 182, 72, 78, 172, 203, 219, 32, 199, 232, 220, 21, 212, 70, 237, 1, 234, 9, 40, 17, 128, 94, 135, 20, 141, 74, 84, 153, 175, 134, 250, 5, 125, 125, 174, 98, 208, 96, 236, 89, 80, 193, 252, 39, 52, 36, 44, 34, 225, 152, 86, 225, 53, 167, 5, 66, 81, 251, 79, 90, 215, 163, 149, 208, 128, 108, 60, 201, 16, 106, 254, 219, 234, 103, 29, 138, 56, 108, 73, 35, 21, 152, 104, 88, 178, 65, 85, 124, 13, 77, 96, 45, 187, 231, 118, 35, 194, 28, 187, 24, 25, 77]

        encrypted = CryptoProtocols.aes_cbc_encrypt(message, key, iv)
        self.assertEqual(encrypted, expected_encrypted)

    def test_aes_cbc_decrypt(self):
        encrypted_text = [65, 41, 148, 201, 53, 1, 39, 10, 17, 138, 192, 250, 0, 10, 192, 91, 238, 149, 239, 179, 168, 141, 36, 242, 116, 85, 194, 148, 184, 78, 225, 79, 128, 75, 200, 184, 191, 44, 56, 63, 179, 214, 247, 114, 26, 157, 251, 164, 89, 76, 194, 98, 74, 78, 37, 138, 59, 181, 0, 11, 76, 180, 12, 55, 69, 98, 1, 181, 187, 48, 70, 228, 93, 132, 81, 209, 3, 222, 240, 246, 53, 239, 247, 95, 98, 154, 193, 65, 202, 255, 32, 144, 101, 24, 227, 56, 139, 13, 42, 45, 22, 135, 118, 40, 86, 85, 39, 222, 18, 29, 111, 121, 102, 173, 249, 14, 22, 186, 249, 70, 90, 81, 129, 50, 135, 38, 161, 56, 135, 201, 26, 121, 167, 58, 21, 60, 176, 33, 197, 104, 54, 214, 72, 179, 36, 198, 20, 38, 109, 251, 197, 244, 139, 160, 22, 89, 5, 132, 229, 47, 56, 10, 158, 149, 57, 58, 37, 152, 179, 193, 103, 130, 189, 228, 174, 58, 230, 169, 250, 42, 63, 152, 105, 15, 120, 47, 91, 117, 160, 93, 241, 103, 125, 95, 238, 87, 154, 195, 175, 73, 157, 210, 196, 98, 17, 246, 212, 69, 168, 230, 248, 85, 154, 165, 78, 234, 47, 127, 97, 72, 232, 225, 133, 206, 210, 102, 169, 184, 162, 173, 182, 232, 201, 97, 104, 23, 149, 107, 164, 84, 225, 195, 84, 154, 21, 251, 196, 161, 137, 178, 67, 194, 217, 245, 188, 224, 109, 211, 208, 49, 169, 126, 40, 238, 91, 164, 126, 27, 33, 235, 30, 66, 106, 210, 45, 33, 33, 18, 183, 93, 188, 80, 233, 225, 56, 129, 201, 142, 84, 136, 208, 22, 77, 39, 3, 43, 97, 210, 112, 245, 109, 195, 255, 59, 210, 80, 88, 176, 64, 136, 131, 54, 58, 184, 204, 43, 161, 181, 77, 150, 189, 149, 19, 201, 224, 111, 116, 163, 84, 170, 197, 196, 152, 68, 124, 111, 41, 25, 35, 96, 55, 105, 147, 226, 27, 1, 44, 46, 61, 70, 219, 92, 126, 220, 172, 17, 106, 42, 87, 185, 240, 47, 250, 207, 62, 56, 206, 111, 169, 25, 120, 209, 159, 203, 34, 34, 112, 240, 23, 184, 213, 197, 128, 41, 20, 230, 252, 5, 34, 38, 76, 243, 68, 230, 51, 184, 95, 147, 195, 178, 122, 100, 254, 73, 189, 1, 243, 80, 134, 59, 4, 253, 169, 34, 157, 139, 136, 215, 18, 168, 15, 63, 78, 172, 34, 16, 48, 177, 239, 146, 214, 169, 151, 178, 159, 222, 121, 71, 26, 108, 251, 110, 100, 131, 6, 178, 240, 154, 82, 121, 129, 213, 84, 236, 8, 51, 47, 90, 15, 227, 93, 67, 74, 124, 204, 20, 69, 25, 193, 121, 53, 44, 123, 3, 122, 189, 64, 183, 228, 169, 62, 161, 226, 75, 186, 80, 160, 102, 224, 78, 173, 61, 166, 62, 97, 103, 207, 218, 77, 141, 254, 64, 243, 94, 16, 165, 199, 66, 177, 9, 128, 169, 86, 0, 52, 98, 7, 153, 116, 192, 167, 15, 158, 185, 155, 226, 60, 138, 48, 152, 209, 169, 19, 25, 106, 88, 13, 225, 110, 2, 65, 74, 115, 74, 67, 159, 208, 83, 75, 52, 214, 76, 24, 29, 244, 141, 176, 164, 72, 169, 141, 7, 210, 221, 33, 74, 78, 175, 238, 40, 17, 147, 78, 23, 61, 156, 128, 182, 95, 128, 27, 215, 131, 235, 245, 38, 10, 80, 155, 213, 58, 118, 221, 14, 34, 200, 14, 146, 116, 10, 233, 190, 207, 126, 131, 59, 171, 110, 28, 17, 36, 166, 194, 30, 65, 188, 111, 138, 14, 158, 29, 67, 203, 64, 96, 35, 53, 152, 1, 77, 122, 161, 24, 215, 82, 227, 255, 25, 68, 252, 127, 158, 41, 230, 85, 76, 46, 39, 194, 71, 158, 140, 245, 132, 210, 91, 102, 34, 212, 62, 179, 59, 218, 242, 86, 145, 101, 58, 211, 27, 12, 171, 147, 48, 127, 154, 81, 187, 174, 242, 45, 200, 11, 96, 174, 56, 176, 64, 120, 180, 59, 171, 75, 36, 224, 171, 208, 150, 211, 253, 35, 123, 134, 184, 131, 139, 23, 61, 41, 77, 251, 245, 123, 251, 8, 167, 61, 150, 142, 123, 239, 31, 54, 188, 229, 91, 2, 153, 37, 86, 222, 49, 167, 113, 4, 174, 168, 48, 69, 158, 197, 9, 129, 136, 100, 34, 241, 216, 198, 16, 144, 2, 113, 181, 202, 27, 156, 68, 30, 25, 228, 72, 247, 19, 75, 107, 136, 60, 75, 248, 59, 187, 134, 35, 50, 134, 177, 88, 16, 182, 72, 78, 172, 203, 219, 32, 199, 232, 220, 21, 212, 70, 237, 1, 234, 9, 40, 17, 128, 94, 135, 20, 141, 74, 84, 153, 175, 134, 250, 5, 125, 125, 174, 98, 208, 96, 236, 89, 80, 193, 252, 39, 52, 36, 44, 34, 225, 152, 86, 225, 53, 167, 5, 66, 81, 251, 79, 90, 215, 163, 149, 208, 128, 108, 60, 201, 16, 106, 254, 219, 234, 103, 29, 138, 56, 108, 73, 35, 21, 152, 104, 88, 178, 65, 85, 124, 13, 77, 96, 45, 187, 231, 118, 35, 194, 28, 187, 24, 25, 77]
        key = bytearray(b'\x9f\x80zI\xbc6\x97\x18\x9e\xf2\xdd\x06\x97N\xc0\xdd')
        iv = bytearray(b'\xc5\xba\x8d\xbf.\xef/>\xa1le}\xa2\xec\xa5\xb7')

        expected_plaintext = """CBC has been the most commonly used mode of operation. Its main drawbacks are that encryption is sequential (i.e., it cannot be parallelized), and that the message must be padded to a multiple of the cipher block size. One way to handle this last issue is through the method known as ciphertext stealing. Note that a one-bit change in a plaintext or initialization vector (IV) affects all following ciphertext blocks.
        Decrypting with the incorrect IV causes the first block of plaintext to be corrupt but subsequent plaintext blocks will be correct. This is because each block is XORed with the ciphertext of the previous block, not the plaintext, so one does not need to decrypt the previous block before using it as the IV for the decryption of the current one. This means that a plaintext block can be recovered from two adjacent blocks of ciphertext."""

        decripted = CryptoProtocols.aes_cbc_decrypt(encrypted_text, key, iv)

        self.assertEqual(expected_plaintext, decripted)


if __name__ == '__main__':
    unittest.main()
