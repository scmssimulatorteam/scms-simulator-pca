# from io import StringIO  # Python3
# import sys

# class OutputGrabber(object):
#     def start(self):
#         print("Redirecting stdout")
#         # Store the reference, in case you want to show things again in standard output
#         self.old_stdout = sys.stdout
#         # This variable will store everything that is sent to the standard output
#         self.result = StringIO()
#         sys.stdout = self.result

#     def stop(self):
#         # Redirect again the std output to screen
#         sys.stdout = self.old_stdout

#     def get_return_value(self):
#         # Then, get the stdout like a string and process it!
#         return "resultado é: " + self.result.getvalue()

import os, sys, select

class OutputGrabber(object):
    def start(self):
        # the pipe would fail for some reason if I didn't write to stdout at some point
        # so I write a space, then backspace (will show as empty in a normal terminal)
        sys.stdout.write(' \b')
        self.pipe_out, self.pipe_in = os.pipe()
        # save a copy of stdout
        self.old_stdout = os.dup(1)
        # replace stdout with our write pipe
        os.dup2(self.pipe_in, 1)

    # check if we have more to read from the pipe
    def more_data(self):
        r, _, _ = select.select([self.pipe_out], [], [], 0)
        return bool(r)

    # read the whole pipe
    def read_pipe(self):
        out = b''
        while self.more_data():
            out += os.read(self.pipe_out, 1024)
        return out.decode("utf-8") 

    def stop(self):
        # put stdout back in place 
        os.dup2(self.old_stdout, 1)