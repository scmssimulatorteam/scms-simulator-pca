from pyrelic.relic_wrapper import RelicUtil
from pyrelic.relic_wrapper import EllipticCurveConfiguration


def setup_relic():
    print("Configurando relic!\n")
    RelicUtil.core_init()
    EllipticCurveConfiguration.param_set_any()
    print("Configurei relic!\n")
