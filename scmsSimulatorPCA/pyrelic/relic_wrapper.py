"""
Wrapper for relic-toolkit library
"""
import ctypes
import ctypes.util
import pyrelic.relic_constants as relic_constants
from pyrelic.relic_utils import wrap_function
from pyrelic.output_grabber import OutputGrabber
import json
import os


out = OutputGrabber()
fileDir = os.path.dirname(os.path.abspath(__file__))
font_file = os.path.join(fileDir, 'relic', 'relic-target', 'lib', 'librelic.so')

if not os.path.exists(font_file):
    print("Não existe arquivo. Criando build...\n")

    falhou = 1
    cwd = os.getcwd()  # get current directory

    os.chdir(fileDir)
    falhou = os.system("make")

    if(falhou):
        print("Falhou o build!\n")
    else:
        print("Funcionou o build!\n")

if os.path.exists(font_file):
    relic = ctypes.CDLL(font_file)
    if not relic._name:
        raise ValueError('Unable to find librelic')
else:
    print("Algo está errado aqui...\n")


# Classes

class RelicUtil():
    __core_init_func = wrap_function(relic, "core_init", ctypes.c_int, None)
    __core_clean_func = wrap_function(relic, "core_clean", ctypes.c_int, None)

    @staticmethod
    def core_init():
        """
        Inicialização da biblioteca. Este método deve ser chamado
        antes de utilizar a biblioteca.
        """
        if (RelicUtil.__core_init_func() != relic_constants.STS_OK):
            RelicUtil.__core_clean_func()
            return False
        else:
            return True


class Hash():
    """
    Hash functions
    """
    __hash_sha256 = wrap_function(relic, "md_map_sh256", None, [ctypes.POINTER(ctypes.c_uint8),
        ctypes.POINTER(ctypes.c_uint8), ctypes.c_int])

    @staticmethod
    def get_hash(original_text: str) -> str:
        """
        Função de Hash default: SHA-256
        """
        return Hash.hash_sha256(original_text)

    @staticmethod
    def hash_sha256(original_text: str) -> str:
        """
        Computes the SHA-256 hash function.
        """
        result = (ctypes.c_uint8 * relic_constants.MD_LEN_SH256)()
        as_bytes = bytearray()
        as_bytes.extend(str(original_text).encode())

        data = (ctypes.c_uint8 * len(as_bytes))(*as_bytes)

        Hash.__hash_sha256(result, data, len(as_bytes))
        result_as_char = ctypes.cast(result, ctypes.c_char_p)
        return result_as_char.value.hex()[:relic_constants.MD_EXIT_HEX_DIGITS]


class BigNumber(ctypes.Structure):
    """
    Interface of the module for multiple precision integer arithmetic.
    """
    __bn_new_func = None
    __bn_print_func = None
    __bn_rand_func = None
    __bn_rand_mod_func = None
    __bn_bits_func = None
    __bn_is_zero_func = None
    __bn_set_bit_func = None
    __bn_copy_func = None
    __bn_set_zero_func = None
    __bn_write_bin_func = None
    __bn_read_bin_func = None
    __bn_size_bin_func = None
    __bn_cmp_func = None
    __bn_add_func = None

    _fields_ = [('alloc', ctypes.c_int),  # The number of digits allocated to this multiple precision integer.
                ('used', ctypes.c_int),  # The number of digits actually used.
                ('sign', ctypes.c_int),  # The sign of this multiple precision integer.
                ('dp', ctypes.c_uint64 * int(relic_constants.BN_SIZE))]  # The sequence of contiguous digits that forms this integer.

    def __init__(self):
        self.dp = (ctypes.c_uint64 * int(relic_constants.BN_SIZE))()

        if not BigNumber.__bn_new_func:
            BigNumber.__bn_new_func = wrap_function(relic, "bn_new_macro", None, [ctypes.POINTER(BigNumber)])
        BigNumber.__bn_new_func(self)

    def __str__(self):
        if not BigNumber.__bn_print_func:
            BigNumber.__bn_print_func = wrap_function(relic, "bn_print", None, [ctypes.POINTER(BigNumber)])

        out.start()
        BigNumber.__bn_print_func(self)
        out.stop()
        return out.read_pipe().rstrip()

    def __eq__(self, other: 'BigNumber'):
        """
        Returns the result of a signed comparison between two multiple precision
        True if they are the same
        """
        if not BigNumber.__bn_cmp_func:
            BigNumber.__bn_cmp_func = wrap_function(relic, "bn_cmp", ctypes.c_int,
                [ctypes.POINTER(BigNumber), ctypes.POINTER(BigNumber)])

        compare = BigNumber.__bn_cmp_func(self, other)
        if compare == relic_constants.CMP_EQ:
            return True
        elif compare == relic_constants.CMP_NE:
            return False
        return None

    def __ne__(self, other: 'BigNumber'):
        """
        Returns the result of a signed comparison between two multiple precision.
        True if they are different.
        """
        if not BigNumber.__bn_cmp_func:
            BigNumber.__bn_cmp_func = wrap_function(relic, "bn_cmp", ctypes.c_int,
                [ctypes.POINTER(BigNumber), ctypes.POINTER(BigNumber)])

        compare = BigNumber.__bn_cmp_func(self, other)
        if compare == relic_constants.CMP_NE:
            return False
        elif compare == relic_constants.CMP_NE:
            return True
        return None

    def rand(self):
        """
        Assigns a random value to a multiple precision integer.

        @param[in] sign          - the sign to be assigned (BN_NEG or BN_POS).
        @param[in] bits          - the number of bits.
        """
        if not BigNumber.__bn_rand_func:
            BigNumber.__bn_rand_func = wrap_function(relic, "bn_rand", None, [ctypes.POINTER(BigNumber), ctypes.c_int, ctypes.c_int])

        BigNumber.__bn_rand_func(self, relic_constants.BN_POS, 255)  # TODO: Rever esse número (255)

    def rand_mod(self, modulus: "BigNumber"):
        """
        Assigns a non-zero random value to a multiple precision integer with absolute
        value smaller than a given modulus.
        @param[in] modulus             - the modulus.

        O valor do módulo corresponde à ordem da curva utilizada (ver Point.get_curve_order)
        """
        if not BigNumber.__bn_rand_mod_func:
            BigNumber.__bn_rand_mod_func = wrap_function(relic, "bn_rand_mod", None, [ctypes.POINTER(BigNumber), ctypes.POINTER(BigNumber)])
        BigNumber.__bn_rand_mod_func(self, modulus)

    def bits(self) -> int:
        """
        Returns the number of bits of a multiple precision integer.
        """
        if not BigNumber.__bn_bits_func:
            BigNumber.__bn_bits_func = wrap_function(relic, "bn_bits", ctypes.c_int, [ctypes.POINTER(BigNumber)])

        bits = BigNumber.__bn_bits_func(self)
        return bits

    def set_zero(self):
        """
        Assigns zero to a multiple precision integer.
        """
        if not BigNumber.__bn_set_zero_func:
            BigNumber.__bn_set_zero_func = wrap_function(relic, "bn_zero", None, [ctypes.POINTER(BigNumber)])

        BigNumber.__bn_set_zero_func(self)

    def is_zero(self) -> bool:
        """
        Tests if a multiple precision integer is zero or not.
        """
        if not BigNumber.__bn_is_zero_func:
            BigNumber.__bn_is_zero_func = wrap_function(relic, "bn_is_zero", ctypes.c_int, [ctypes.POINTER(BigNumber)])

        iszero = BigNumber.__bn_is_zero_func(self)
        if iszero == 0:
            return False
        elif iszero == 1:
            return True
        return None

    def set_bit(self, position: int, value: int):
        """
        Stores a bit in a given position on a multiple precision integer.

        @param[in] position      - the bit position to store.
        @param[in] value         - the bit value.
        """
        if not BigNumber.__bn_set_bit_func:
            BigNumber.__bn_set_bit_func = wrap_function(relic, "bn_set_bit", None, [ctypes.POINTER(BigNumber), ctypes.c_int, ctypes.c_int])
        BigNumber.__bn_set_bit_func(self, ctypes.c_int(position), ctypes.c_int(value))

    def copy(self, big_number_copy: 'BigNumber'):
        """
        Copies the second argument to the first argument.

        @param[in] big_number_copy       - the multiple precision integer to copy.
        """
        if not BigNumber.__bn_copy_func:
            BigNumber.__bn_copy_func = wrap_function(relic, "bn_copy", None, [ctypes.POINTER(BigNumber), ctypes.POINTER(BigNumber)])
        BigNumber.__bn_copy_func(big_number_copy, self)

    def read_bin(self, value_to_read: list):
        """
        Reads a positive multiple precision integer from a byte vector in big-endian format.
        """
        size = len(value_to_read)
        bin_to_read = (ctypes.c_uint8 * size)(*value_to_read)

        if not BigNumber.__bn_read_bin_func:
            BigNumber.__bn_read_bin_func = wrap_function(relic, "bn_read_bin", None, [ctypes.POINTER(BigNumber), ctypes.POINTER(ctypes.c_uint8), ctypes.c_int])
        BigNumber.__bn_read_bin_func(self, bin_to_read, size)

    def write_bin(self) -> list:
        """
        Writes a positive multiple precision integer to a byte vector in big-endian format.
        """
        if not BigNumber.__bn_write_bin_func:
            BigNumber.__bn_write_bin_func = wrap_function(relic, "bn_write_bin", None, [ctypes.POINTER(ctypes.c_uint8), ctypes.c_int, ctypes.POINTER(BigNumber)])
        size = self.size_bin()
        bin_to_write = (ctypes.c_uint8 * size)()
        BigNumber.__bn_write_bin_func(bin_to_write, size, self)

        written_bin_list = [bin_to_write[i] for i in range(size)]
        return written_bin_list

    def size_bin(self) -> int:
        """
        Returns the number of bytes necessary to store a multiple precision integer.
        """
        if not BigNumber.__bn_size_bin_func:
            BigNumber.__bn_size_bin_func = wrap_function(relic, "bn_size_bin", ctypes.c_int, [ctypes.POINTER(BigNumber)])

        size = BigNumber.__bn_size_bin_func(self)
        return size

    @staticmethod
    def add(result: "BigNumber", numberA: "BigNumber", numberB: "BigNumber"):
        """
        Adds two multiple precision integers. Computes c = a + b.
        """
        if not BigNumber.__bn_add_func:
            BigNumber.__bn_add_func = wrap_function(relic, "bn_add", None, [ctypes.POINTER(BigNumber), ctypes.POINTER(BigNumber), ctypes.POINTER(BigNumber)])

        BigNumber.__bn_add_func(result, numberA, numberB)


class EllipticCurveConfiguration():
    __ec_param_set_any_func = wrap_function(relic, "ec_param_set_any_macro", ctypes.c_int, None)
    __ec_param_print_func = wrap_function(relic, "ec_param_print_macro", None, None)
    __ec_param_level_func = wrap_function(relic, "ec_param_level_macro", ctypes.c_int, None)

    @staticmethod
    def param_set_any() -> bool:
        """
        Configures some set of curve parameters for the current security level.
        """
        result = EllipticCurveConfiguration.__ec_param_set_any_func()
        if result == relic_constants.STS_OK:
            return True
        else:
            return False

    @staticmethod
    def param_print():
        """
        Prints the current configured prime elliptic curve.
        """
        EllipticCurveConfiguration.__ec_param_print_func()

    @staticmethod
    def param_security_level() -> int:
        """
        Returns the current security level.
        """
        return EllipticCurveConfiguration.__ec_param_level_func()


class Point(ctypes.Structure):
    """
    Elliptic curve cryptography
    Representação de um ponto na curva elíptica.
    """
    __ec_new_func = None
    __ec_rand_func = None
    __ec_print_func = None
    __ec_is_valid_func = None
    __ec_add_func = None
    __ec_mul_func = None
    __ec_mul_gen_func = None
    __ec_mul_sim_func = None
    __ec_mul_gen_sim_func = None
    __ec_curve_get_ord_func = None
    __ec_read_bin_func = None
    __ec_write_bin_func = None
    __ec_size_bin_func = None
    __ec_ec_cmp_func = None
    __ec_norm_func = None

    _fields_ = [('x', ctypes.c_uint64 * int(relic_constants.FP_DIGS)),  # The first coordinate.
                ('y', ctypes.c_uint64 * int(relic_constants.FP_DIGS)),  # The second coordinate.
                ('z', ctypes.c_uint64 * int(relic_constants.FP_DIGS)),  # The third coordinate (projective representation).
                ('norm', ctypes.c_int)]  # Flag to indicate that this point is normalized

    def __init__(self):
        self.x = (ctypes.c_uint64 * int(relic_constants.FP_DIGS))()
        self.y = (ctypes.c_uint64 * int(relic_constants.FP_DIGS))()
        self.z = (ctypes.c_uint64 * int(relic_constants.FP_DIGS))()

        if not Point.__ec_new_func:
            Point.__ec_new_func = wrap_function(relic, "ec_new_macro", None, [ctypes.POINTER(Point)])
        Point.__ec_new_func(self)

    def __str__(self):
        """
        Prints a prime elliptic curve point.
        """
        if not Point.__ec_print_func:
            Point.__ec_print_func = wrap_function(relic, "ec_print_macro", None, [ctypes.POINTER(Point)])

        out.start()
        Point.__ec_print_func(self)
        out.stop()
        return out.read_pipe().rstrip()

    def __eq__(self, other: 'Point'):
        """
        Compares two prime elliptic curve points.
        Returns True if the curve points are the same.
        """
        if not Point.__ec_ec_cmp_func:
            Point.__ec_ec_cmp_func = wrap_function(relic, "ec_cmp_macro", ctypes.c_int,
                [ctypes.POINTER(Point), ctypes.POINTER(Point)])

        compare = Point.__ec_ec_cmp_func(self, other)
        if compare == relic_constants.CMP_EQ:
            return True
        elif compare == relic_constants.CMP_NE:
            return False
        return None

    def __ne__(self, other: 'Point'):
        """
        Compares two prime elliptic curve points.
        Returns True if the curve points are different.
        """
        if not Point.__ec_ec_cmp_func:
            Point.__ec_ec_cmp_func = wrap_function(relic, "ec_cmp_macro", ctypes.c_int,
                [ctypes.POINTER(Point), ctypes.POINTER(Point)])

        compare = Point.__ec_ec_cmp_func(self, other)
        if compare == relic_constants.CMP_NE:
            return False
        elif compare == relic_constants.CMP_NE:
            return True
        return None

    def normalize_point(self):
        if not Point.__ec_norm_func:
            Point.__ec_norm_func = wrap_function(relic, "ec_norm_macro", None, [ctypes.POINTER(Point),
                ctypes.POINTER(Point)])
        result = Point()
        Point.__ec_norm_func(result, self)
        return result

    def rand(self):
        """
        Assigns a random value to a prime elliptic curve point.
        """
        if not Point.__ec_rand_func:
            Point.__ec_rand_func = wrap_function(relic, "ec_rand_macro", None, [ctypes.POINTER(Point)])

        Point.__ec_rand_func(self)

    def is_valid(self) -> bool:
        """
        Tests if a point is in the curve.
        """
        if not Point.__ec_is_valid_func:
            Point.__ec_is_valid_func = wrap_function(relic, "ec_is_valid_macro", ctypes.c_int, [ctypes.POINTER(Point)])

        is_valid = Point.__ec_is_valid_func(self)
        if is_valid == 0:
            return False
        elif is_valid == 1:
            return True
        return None

    @staticmethod
    def get_curve_order(order: BigNumber):
        """
        Returns the order of the group of points in the prime elliptic curve.
        """
        if not Point.__ec_curve_get_ord_func:
            Point.__ec_curve_get_ord_func = wrap_function(relic, "ec_curve_get_ord_macro", None, [ctypes.POINTER(BigNumber)])
        Point.__ec_curve_get_ord_func(order)

    @staticmethod
    def add(result: "Point", pointA: "Point", pointB: "Point"):
        if not Point.__ec_add_func:
            Point.__ec_add_func = wrap_function(relic, "ec_add_macro", None, [ctypes.POINTER(Point), ctypes.POINTER(Point), ctypes.POINTER(Point)])

        Point.__ec_add_func(result, pointA, pointB)

    @staticmethod
    def multiply(result: "Point", pointP: "Point", numberK: BigNumber):
        """
        Multiplies a prime elliptic curve point by an integer. Computes R = kP.
        """
        if not Point.__ec_mul_func:
            Point.__ec_mul_func = wrap_function(relic, "ec_mul_macro", None, [ctypes.POINTER(Point), ctypes.POINTER(Point), ctypes.POINTER(BigNumber)])

        Point.__ec_mul_func(result, pointP, numberK)

    @staticmethod
    def multiply_by_generator(result: "Point", numberK: BigNumber):
        """
        Multiplies the generator of a prime elliptic curve by an integer.
        """
        if not Point.__ec_mul_gen_func:
            Point.__ec_mul_gen_func = wrap_function(relic, "ec_mul_gen_macro", None, [ctypes.POINTER(Point), ctypes.POINTER(BigNumber)])

        Point.__ec_mul_gen_func(result, numberK)

    @staticmethod
    def multiply_simul(result: "Point", pointP: "Point", numberK: BigNumber,
                        pointQ: "Point", numberM: BigNumber):
        """
        Multiplies and adds two prime elliptic curve points simultaneously. Computes R = kP + mQ.
        """
        if not Point.__ec_mul_sim_func:
            Point.__ec_mul_sim_func = wrap_function(relic, "ec_mul_sim_macro", None, [ctypes.POINTER(Point), ctypes.POINTER(Point),
                ctypes.POINTER(BigNumber), ctypes.POINTER(Point), ctypes.POINTER(BigNumber)])
        Point.__ec_mul_sim_func(result, pointP, numberK, pointQ, numberM)

    @staticmethod
    def multiply_by_generator_simul(result: "Point", numberK: BigNumber, pointP: "Point", numberM: BigNumber):
        """
        Multiplies and adds the generator and a prime elliptic curve point simultaneously. Computes R = kG + mQ.
        """
        if not Point.__ec_mul_gen_sim_func:
            Point.__ec_mul_gen_sim_func = wrap_function(relic, "ec_mul_sim_gen_macro", None, [ctypes.POINTER(Point), ctypes.POINTER(BigNumber),
                ctypes.POINTER(Point), ctypes.POINTER(BigNumber)])
        Point.__ec_mul_gen_sim_func(result, numberK, pointP, numberM)

    def read_bin(self, value_to_read: list):
        """
        Reads an elliptic curve point from a byte vector in big-endian format.
        """
        if not Point.__ec_read_bin_func:
            Point.__ec_read_bin_func = wrap_function(relic, "ec_read_bin_macro", None, [ctypes.POINTER(Point), ctypes.POINTER(ctypes.c_uint8), ctypes.c_int])

        size = len(value_to_read)
        bin_to_read = (ctypes.c_uint8 * size)(*value_to_read)
        Point.__ec_read_bin_func(self, bin_to_read, size)

    def write_bin(self) -> list:
        """
         Writes an elliptic curve point to a byte vector in big-endian format with optional point compression.
        """
        if not Point.__ec_write_bin_func:
            Point.__ec_write_bin_func = wrap_function(relic, "ec_write_bin_macro", None, [ctypes.POINTER(ctypes.c_uint8), ctypes.c_int, ctypes.POINTER(Point), ctypes.c_int])

        size = self.size_bin()
        bin_to_write = (ctypes.c_uint8 * size)()
        Point.__ec_write_bin_func(bin_to_write, size, self, relic_constants.ec_compress)

        written_bin_list = [bin_to_write[i] for i in range(size)]
        return written_bin_list

    def size_bin(self) -> int:
        """
        Returns the number of bytes necessary to store an elliptic curve point with optional point compression.
        """
        if not Point.__ec_size_bin_func:
            Point.__ec_size_bin_func = wrap_function(relic, "ec_size_bin_macro", ctypes.c_int, [ctypes.POINTER(Point), ctypes.c_int])

        size = Point.__ec_size_bin_func(self, relic_constants.ec_compress)
        return size


class CryptoProtocols():
    """
    Implementation of the Cryptographic protocols
    """
    __cp_key_gen = None
    __cp_ecies_enc = None
    __cp_ecies_dec = None
    __cp_ecdsa_sig = None
    __cp_ecdsa_ver = None
    __bc_aes_enc = None
    __bc_aes_dec = None

    @staticmethod
    def generate_keys(privateKey: BigNumber, publicKey: Point):
        """
        Generates a key pair.
        """
        if not CryptoProtocols.__cp_key_gen:
            CryptoProtocols.__cp_key_gen = wrap_function(relic, "cp_ecies_gen", None, [ctypes.POINTER(BigNumber), ctypes.POINTER(Point)])

        CryptoProtocols.__cp_key_gen(privateKey, publicKey)

    @staticmethod
    def ecies_encrypt(publicKey: Point, value_to_encrypt: dict):
        """
        Encapsulamento para a função:
            Encrypts using the ECIES cryptosystem.
            @param[out] r            - the resulting elliptic curve point.
            @param[out] out          - the output buffer.
            @param[in, out] out_len  - the buffer capacity and number of bytes written.
            @param[in] in            - the input buffer.
            @param[in] in_len        - the number of bytes to encrypt.
            @param[in] q             - the public key.

            @return STS_OK if no errors occurred, STS_ERR otherwise.
            Function returns encrypted value and reconstruction point.

        Retorna array de ctypes.c_uint8 com o valor cifrado e
        o ponto resultante da operação.
        """
        if not CryptoProtocols.__cp_ecies_enc:
            CryptoProtocols.__cp_ecies_enc = wrap_function(relic, "cp_ecies_enc", ctypes.c_int, [ctypes.POINTER(Point), ctypes.POINTER(ctypes.c_uint8), ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_uint8), ctypes.c_int, ctypes.POINTER(Point)])

        ponto_resultante = Point()

        value_as_str = json.dumps(value_to_encrypt)
        value_as_bytes = bytearray()
        value_as_bytes.extend(str(value_as_str).encode())
        in_value = (ctypes.c_uint8 * len(value_as_bytes))(*value_as_bytes)

        out_len_int = 3 * len(value_as_bytes)  # Tamanho maior do que a saida (tamanho real será escrito na variável out_len)
        out_len = ctypes.c_int(out_len_int)
        bytes_written = out_len.value
        out_buf = (ctypes.c_uint8 * out_len_int)()

        sucesso = CryptoProtocols.__cp_ecies_enc(ponto_resultante, out_buf, out_len, in_value, len(in_value), publicKey)

        if(sucesso == relic_constants.STS_OK):
            bytes_written = out_len.value
            lista = out_buf[:bytes_written]
            return lista, ponto_resultante
        else:
            return None, None

    @staticmethod
    def ecies_decrypt(reconstruction_point: Point, encrypted_value: str, privateKey: BigNumber) -> dict:
        """
        Encapsulamento para a função:
            Decrypts using the ECIES cryptosystem.

            RELIC CONFIGURATION
            @param[out] out          - the output buffer.
            @param[in, out] out_len  - the buffer capacity and number of bytes written.
            @param[in] in            - the input buffer.
            @param[in] in_len        - the number of bytes to encrypt.
            @param[in] iv            - the block cipher initialization vector.
            @param[in] d             - the private key.
            @param[in] r            - the resulting elliptic curve point.
            @return STS_OK if no errors occurred, STS_ERR otherwise.

        Retorna um dicionário correspondente à entrada decifrada
        """

        if not CryptoProtocols.__cp_ecies_dec:
            CryptoProtocols.__cp_ecies_dec = wrap_function(relic, "cp_ecies_dec", ctypes.c_int,
             [ctypes.POINTER(ctypes.c_uint8), ctypes.POINTER(ctypes.c_int), ctypes.POINTER(Point),
             ctypes.POINTER(ctypes.c_uint8), ctypes.c_int, ctypes.POINTER(BigNumber)])

        encrypted_value_size = len(encrypted_value)
        in_buf = (ctypes.c_uint8 * len(encrypted_value))(*encrypted_value)
        out_len = ctypes.c_int(3 * encrypted_value_size)
        out_buf = (ctypes.c_uint8 * out_len.value)()

        sucesso = CryptoProtocols.__cp_ecies_dec(out_buf, out_len, reconstruction_point,
            in_buf, len(in_buf), privateKey)

        if(sucesso == relic_constants.STS_OK):
            result_as_char = ctypes.cast(out_buf, ctypes.c_char_p)
            result_as_str = result_as_char.value.decode("utf-8")
            returning_dict = json.loads(result_as_str)  # Para retornar dict
            return returning_dict
        else:
            return None

    @staticmethod
    def ecdsa_sign(content_to_sign: str, privateKey: BigNumber):
        """
        Encapsulamento para a função:
            Signs a message using ECDSA.
            @param[out] r                - the first component of the signature.
            @param[out] s                - the second component of the signature.
            @param[in] msg               - the message to sign.
            @param[in] len               - the message length in bytes.
            @param[in] hash              - the flag to indicate the message format.
            @param[in] d                 - the private key.
            @return STS_OK if no errors occurred, STS_ERR otherwise.

        Retorna dois BigNumbers correspondentes à assinatura
        """
        if not CryptoProtocols.__cp_ecdsa_sig:
            CryptoProtocols.__cp_ecdsa_sig = wrap_function(relic, "cp_ecdsa_sig", ctypes.c_int,
                [ctypes.POINTER(BigNumber), ctypes.POINTER(BigNumber), ctypes.POINTER(ctypes.c_uint8),
                ctypes.c_int, ctypes.c_int, ctypes.POINTER(BigNumber)])

        value_as_str = json.dumps(content_to_sign)
        value_as_bytes = bytearray()
        value_as_bytes.extend(str(value_as_str).encode())
        msg = (ctypes.c_uint8 * len(value_as_bytes))(*value_as_bytes)

        is_hash = 0  # False

        r = BigNumber()
        s = BigNumber()
        sucesso = CryptoProtocols.__cp_ecdsa_sig(r, s, msg, len(msg), is_hash, privateKey)

        if(sucesso == relic_constants.STS_OK):
            return r, s
        else:
            return None, None

    @staticmethod
    def ecdsa_ver(content_to_verify: str, sign1: BigNumber, sign2: BigNumber, publicKey: BigNumber):
        """
        Encapsulamento para a função:
            Verifies a message signed with ECDSA using the basic method.
            @param[out] r                - the first component of the signature.
            @param[out] s                - the second component of the signature.
            @param[in] msg               - the message to sign.
            @param[in] len               - the message length in bytes.
            @param[in] hash              - the flag to indicate the message format.
            @param[in] q                 - the public key.
            @return a boolean value indicating if the signature is valid.

        Aceita como entrada os dois BigNumbers da assinatura e a chave pública para verificação.
        Retorna um bool informando se a chave pública corresponde à assinatura.
        """
        if not CryptoProtocols.__cp_ecdsa_ver:
            CryptoProtocols.__cp_ecdsa_ver = wrap_function(relic, "cp_ecdsa_ver", ctypes.c_int,
                [ctypes.POINTER(BigNumber), ctypes.POINTER(BigNumber), ctypes.POINTER(ctypes.c_uint8),
                ctypes.c_int, ctypes.c_int, ctypes.POINTER(Point)])

        value_as_str = json.dumps(content_to_verify)
        value_as_bytes = bytearray()
        value_as_bytes.extend(str(value_as_str).encode())
        msg = (ctypes.c_uint8 * len(value_as_bytes))(*value_as_bytes)

        is_hash = 0  # False

        sucesso = CryptoProtocols.__cp_ecdsa_ver(sign1, sign2, msg, len(msg), is_hash, publicKey)
        if(sucesso == 1):
            return True
        else:
            return False

    @staticmethod
    def aes_cbc_encrypt(value_to_encrypt: dict, key_bytes: bytes, iv_bytes: bytes):
        """
        Recebe o texto às claras, a chave e o vetor de inicialização.
        A chave (128 bits) e o IV devem ser valores de 16 bytes obtidos
        com uma função pseudoaleatória segura, como os.urandom(16).

        Encapsulamento para a função:
            Encrypts with AES in CBC mode.

            @param[out] out          - the resulting ciphertext.
            @param[in,out] out_len   - the buffer capacity and number of bytes written.
            @param[in] in            - the bytes to be encrypted.
            @param[in] in_len        - the number of bytes to encrypt.
            @param[in] key           - the key.
            @param[in] key_len       - the key size in bits.
            @return STS_OK if no errors occurred, STS_ERR otherwise.

        Retorna array de ctypes.c_uint8 com o valor cifrado e
        o ponto resultante da operação.
        """
        if not CryptoProtocols.__bc_aes_enc:
            CryptoProtocols.__bc_aes_enc = wrap_function(relic, "bc_aes_cbc_enc", ctypes.c_int,
                [ctypes.POINTER(ctypes.c_uint8), ctypes.POINTER(ctypes.c_int),  # out e out_len
                ctypes.POINTER(ctypes.c_uint8), ctypes.c_int,  # in e in_len
                ctypes.POINTER(ctypes.c_uint8), ctypes.c_int,  # key e key_len (bits)
                ctypes.POINTER(ctypes.c_uint8)])  # iv

        value_as_str = json.dumps(value_to_encrypt)
        value_as_bytes = bytearray()
        value_as_bytes.extend(str(value_as_str).encode())
        in_value = (ctypes.c_uint8 * len(value_as_bytes))(*value_as_bytes)

        out_len_int = (len(value_as_str) + 1) * 16  # Tamanho da saída do AES em bytes (value_to_encrypt em bytes) (block size of 16-bytes)
        out_len = ctypes.c_int(out_len_int)
        out_buf = (ctypes.c_uint8 * out_len_int)()

        key_bytearray = bytearray(key_bytes)
        key = (ctypes.c_uint8 * len(key_bytearray))(*key_bytearray)

        iv_bytearray = bytearray(iv_bytes)
        iv = (ctypes.c_uint8 * len(iv_bytearray))(*iv_bytearray)

        sucesso = CryptoProtocols.__bc_aes_enc(out_buf, out_len, in_value, len(in_value), key, 8 * len(key), iv)
        if(sucesso == relic_constants.STS_OK):
            bytes_written = out_len.value
            lista = out_buf[:bytes_written]
            return lista
        else:
            return None

    @staticmethod
    def aes_cbc_decrypt(encrypted_value: dict, key_bytes: bytes, iv_bytes: bytes):
        """
        Encapsulamento para a função:
            Decrypts with AES in CBC mode.

            @param[out] out          - the resulting plaintext.
            @param[in,out] out_len   - the buffer capacity and number of bytes written.
            @param[in] in            - the bytes to be decrypted.
            @param[in] in_len        - the number of bytes to decrypt.
            @param[in] key           - the key.
            @param[in] key_len       - the key size in bits.
            @return STS_OK if no errors occurred, STS_ERR otherwise.

        Retorna um dicionário correspondente à entrada decifrada
        """
        if not CryptoProtocols.__bc_aes_dec:
            CryptoProtocols.__bc_aes_dec = wrap_function(relic, "bc_aes_cbc_dec", ctypes.c_int,
                [ctypes.POINTER(ctypes.c_uint8), ctypes.POINTER(ctypes.c_int),  # out e out_len
                ctypes.POINTER(ctypes.c_uint8), ctypes.c_int,  # in e in_len
                ctypes.POINTER(ctypes.c_uint8), ctypes.c_int,  # key e key_len (bits)
                ctypes.POINTER(ctypes.c_uint8)])  # iv

        encrypted_value_size = len(encrypted_value)
        in_value = (ctypes.c_uint8 * len(encrypted_value))(*encrypted_value)

        out_len = ctypes.c_int(encrypted_value_size)
        out_buf = (ctypes.c_uint8 * out_len.value)()

        key_in_bytes = bytearray(key_bytes)
        key = (ctypes.c_uint8 * len(key_in_bytes))(*key_in_bytes)

        iv_in_bytes = bytearray(iv_bytes)
        iv = (ctypes.c_uint8 * len(iv_in_bytes))(*iv_in_bytes)

        sucesso = CryptoProtocols.__bc_aes_dec(out_buf, out_len, in_value, len(in_value), key, 8 * len(key_in_bytes), iv)

        if(sucesso == relic_constants.STS_OK):
            result_as_char = ctypes.cast(out_buf, ctypes.c_char_p)
            result_as_str = result_as_char.value.decode("utf-8")
            returning_dict = json.loads(result_as_str)  # Para retornar dict
            return returning_dict
        else:
            return None
