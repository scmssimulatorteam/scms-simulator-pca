# chat/consumers.py
from channels.generic.websocket import WebsocketConsumer
from pca import scms_cryptography
from scmsSimulatorPCA.settings.base import CURRENT_SETTING, FIRST_MODE, SECOND_MODE, THIRD_MODE
import json


class PcaConsumer(WebsocketConsumer):
    def connect(self):
        self.accept()

    def disconnect(self, close_code):
        pass

    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        # print("Mensagem recebida: %s" % text_data_json)

        if('cyphertext' in text_data_json and 'recovery_point' in text_data_json):
            if(CURRENT_SETTING == FIRST_MODE):
                return_message = scms_cryptography.sign_certificate_first_cenario(text_data_json)
            elif(CURRENT_SETTING == SECOND_MODE):
                return_message = scms_cryptography.sign_certificate_second_cenario(text_data_json)
            elif(CURRENT_SETTING == THIRD_MODE):
                return_message = scms_cryptography.sign_certificate_third_cenario(text_data_json)
            else:
                return_message = "Algo não está certo!"
        else:
            return_message = "Retornei..."
        # print("Enviando: %s" % return_message)
        self.send(text_data=json.dumps({
            'message': return_message
        }))
