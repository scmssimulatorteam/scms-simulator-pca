from django.shortcuts import render
from scmsSimulatorPCA.settings.base import CURRENT_SETTING, FIRST_MODE, SECOND_MODE, THIRD_MODE
from .models import VehicleCertificate
from .models import VehicleCertificateSeeded
from .models import VehicleCertificateButterfly
import datetime


def index(request):
    if(CURRENT_SETTING == FIRST_MODE):
        valid_certificates = VehicleCertificate.objects.filter(certificate__expiration_date__gt=datetime.date.today()).order_by('-certificate__creation_date', 'identification_code')
        expired_certificates = VehicleCertificate.objects.filter(certificate__expiration_date__lte=datetime.date.today()).order_by('-certificate__creation_date', 'identification_code')

        context = {
            'valid_certificates': valid_certificates,
            'expired_certificates': expired_certificates
        }
        return render(request, 'pca/index_mode_1.html', context)
    elif(CURRENT_SETTING == SECOND_MODE):
        certificates = VehicleCertificateSeeded.objects.all().order_by('-certificate__creation_date', 'request_id')

        context = {
            'certificates': certificates
        }
        return render(request, 'pca/index_mode_2.html', context)
    elif(CURRENT_SETTING == THIRD_MODE):
        valid_certificates = VehicleCertificateButterfly.objects.all().order_by('-certificate__creation_date', 'request_id')

        context = {
            'certificates': valid_certificates
        }
        return render(request, 'pca/index_mode_3.html', context)
    else:
        return render(request, 'error.html', context)
