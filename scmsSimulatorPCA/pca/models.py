from django.db import models
from pyrelic.relic_constants import BIN_BN_SIZE
from pyrelic.relic_constants import BIN_EC_SIZE
from pyrelic.relic_constants import LINKAGE_SIZE
from django.core.validators import int_list_validator
import datetime


class PrivateKey(models.Model):
    bin_value = models.CharField(max_length=BIN_BN_SIZE, validators=[int_list_validator])


class PublicKey(models.Model):
    bin_value = models.CharField(max_length=BIN_EC_SIZE, validators=[int_list_validator])


class OwnerPublicKey(models.Model):
    """
        PCA só terá:
        - sua própria chave pública (PCA)
        - chave pública da RA (no segundo e terceiro modo de operação)
    """
    PROFILES = (
        ('P', 'PCA'),
        ('R', 'RA'),
        ('V', 'Vehicle')
    )

    belong_to = models.CharField(max_length=1, choices=PROFILES)
    public_key = models.ForeignKey(PublicKey, on_delete=models.CASCADE)


class KeyPair(models.Model):
    public_key = models.ForeignKey(PublicKey, on_delete=models.CASCADE)
    private_key = models.ForeignKey(PrivateKey, on_delete=models.CASCADE)


def get_deadline():
    return datetime.date.today() + datetime.timedelta(days=7)


class Certificate(models.Model):
    creation_date = models.DateTimeField(auto_now_add=True)
    expiration_date = models.DateField(default=get_deadline)
    public_key = models.ForeignKey(PublicKey, on_delete=models.CASCADE)
    signature_value_1 = models.CharField(max_length=BIN_BN_SIZE, validators=[int_list_validator])
    signature_value_2 = models.CharField(max_length=BIN_BN_SIZE, validators=[int_list_validator])


class VehicleCertificate(models.Model):
    certificate = models.ForeignKey(Certificate, on_delete=models.CASCADE)
    identification_code = models.CharField(max_length=BIN_EC_SIZE)


class VehicleCertificateSeeded(models.Model):
    certificate = models.ForeignKey(Certificate, on_delete=models.CASCADE)
    pseud_point = models.CharField(max_length=BIN_EC_SIZE, validators=[int_list_validator])
    cypher_point = models.CharField(max_length=BIN_EC_SIZE, validators=[int_list_validator])
    request_id = models.CharField(max_length=BIN_EC_SIZE)
    seed = models.CharField(max_length=BIN_BN_SIZE, validators=[int_list_validator])


class VehicleCertificateButterfly(models.Model):
    request_id = models.CharField(max_length=BIN_EC_SIZE)
    certificate = models.ForeignKey(Certificate, on_delete=models.CASCADE)
    pseud_point = models.CharField(max_length=BIN_EC_SIZE, validators=[int_list_validator])
    cypher_point = models.CharField(max_length=BIN_EC_SIZE, validators=[int_list_validator])
    seed = models.CharField(max_length=BIN_BN_SIZE, validators=[int_list_validator])
    pre_linkage_value_1 = models.CharField(max_length=LINKAGE_SIZE, validators=[int_list_validator])
    pre_linkage_value_2 = models.CharField(max_length=LINKAGE_SIZE, validators=[int_list_validator])
    linkage_value = models.CharField(max_length=LINKAGE_SIZE, validators=[int_list_validator])
