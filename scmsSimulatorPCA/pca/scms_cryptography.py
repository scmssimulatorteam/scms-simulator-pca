from django.db import transaction
from pyrelic.relic_wrapper import BigNumber
from pyrelic.relic_wrapper import Point
from pyrelic.relic_wrapper import CryptoProtocols
from pyrelic.relic_wrapper import Hash
from . import bd_conversion


@transaction.atomic
def sign_certificate_first_cenario(vehicle_request):
    """
        PRIMEIRO MODO DE OPERAÇÃO

        Função para PCA assinar certificados.
        Recebe mensagem com:
            - 'recovery_point': ponto para decifrar o texto recebido,
            - 'cyphertext': valores cifrados do certificado.

        Após a decifração de cyphertext, obtemos a estrutura:
            - 'vehicle_id': Chave pública vitálicia do veículo
            - 'pseud_point': Ponto que deve ser assinado,
            - 'cypher_point': Ponto que deve cifrar o pacote de retorno

        Assina o hash do valor binário de pseud_point e envia
        {
            'recovery_point': ponto para decifrar o texto enviado,
            'cyphertext': texto cifrado
        }

        E cyphertext, por sua vez, corresponde a:
            'sign_1' e 'sign_2', dois BigNumbers correspondentes a assinatura

        A PCA armazena todos os valores gerados e pode assim rastrear o veículo.
    """
    private, public = get_own_keys()

    bin_value = vehicle_request.get('recovery_point')
    recovery_point = Point()
    recovery_point.read_bin(bin_value)

    cyphertext = vehicle_request.get('cyphertext')

    decrypted_message = CryptoProtocols.ecies_decrypt(recovery_point, cyphertext, private)

    identification = decrypted_message.get('vehicle_id')
    pseudonym_bin = decrypted_message.get('pseud_point')

    cypherkey = Point()
    cypherkey.read_bin(decrypted_message.get('cypher_point'))

    signature_1, signature_2 = CryptoProtocols.ecdsa_sign(pseudonym_bin, private)

    content = {
        'sign_1': signature_1.write_bin(),
        'sign_2': signature_2.write_bin()
    }

    # Salvar no banco de dados o certificado do veículo (comportamento malicioso)
    bd_conversion.save_vehicle_certificate(identification, pseudonym_bin, signature_1, signature_2)

    cyphertext, point = CryptoProtocols.ecies_encrypt(cypherkey, content)
    return_message = {
        'cyphertext': cyphertext,
        'recovery_point': point.write_bin()
    }

    return return_message


@transaction.atomic
def sign_certificate_second_cenario(vehicle_request):
    """
        SEGUNDO MODO DE OPERAÇÃO

        Função para PCA assinar certificados.
        Recebe mensagem com:
            - 'recovery_point': ponto para decifrar o texto recebido,
            - 'cyphertext': valores cifrados do certificado.

        Após a decifração de cyphertext, obtemos a estrutura:
            - 'request_id': Código de identificação do pedido
            - 'pseud_point': Ponto que deve ser assinado,
            - 'cypher_point': Ponto que deve cifrar o pacote de retorno

        Gera um bigNumber aleatório "seed" e o multiplica
        pelo valor do ponto gerador, dando origem ao ponto C.
        O pseudônimo corresponde aos valores:
            pseudonym = pseud_point + C

        Assina o hash do valor binário de pseudonym e forma o pacote:
        {
            'sign_1',
            'sign_2', # dois BigNumbers correspondentes a assinatura
            'seed'
        }
        Cifra tudo com cypher_point e obtém o pacote:
        {
            'request_id': valor recebido do request_id
            'recovery_point': ponto para decifrar o texto enviado,
            'cyphertext': texto cifrado
        }
        Esse pacote é cifrado e enviado pra RA.

        A PCA armazena todos valores, mas não consegue rastrear o veículo.
    """
    private, public = get_own_keys()

    bin_value = vehicle_request.get('recovery_point')
    recovery_point = Point()
    recovery_point.read_bin(bin_value)

    cyphertext = vehicle_request.get('cyphertext')

    decrypted_message = CryptoProtocols.ecies_decrypt(recovery_point, cyphertext, private)

    identification = decrypted_message.get('request_id')
    pseudonym_bin = decrypted_message.get('pseud_point')

    cypherkey = Point()
    cypherkey.read_bin(decrypted_message.get('cypher_point'))

    pseud_origin_point = Point()
    pseud_origin_point.read_bin(pseudonym_bin)

    # Obtém a ordem da curva utilizada
    mod = BigNumber()
    Point.get_curve_order(mod)

    # Calcula uma semente c aleatória
    seed = BigNumber()
    seed.rand_mod(mod)

    # Obtém C = cG
    point_C = Point()
    Point.multiply_by_generator(point_C, seed)

    final_pseudonym = Point()
    Point.add(final_pseudonym, pseud_origin_point, point_C)
    final_pseudonym_bin = final_pseudonym.write_bin()

    signature_1, signature_2 = CryptoProtocols.ecdsa_sign(final_pseudonym_bin, private)

    content = {
        'seed': seed.write_bin(),
        'sign_1': signature_1.write_bin(),
        'sign_2': signature_2.write_bin()
    }

    # Salvar no banco de dados o certificado do veículo (comportamento malicioso)
    bd_conversion.save_vehicle_certificate_with_seed(identification, seed,
        final_pseudonym, cypherkey, pseud_origin_point, signature_1, signature_2)

    # bd_conversion.save_vehicle_certificate(identification, pseudonym_bin, signature_1, signature_2)
    cyphertext_vehicle, point_vehicle = CryptoProtocols.ecies_encrypt(cypherkey, content)

    plain_package = {
        'request_id': identification,
        'cyphertext': cyphertext_vehicle,
        'recovery_point': point_vehicle.write_bin()
    }
    chave_RA = bd_conversion.get_RA_public_key()
    cyphertext_final, point_final = CryptoProtocols.ecies_encrypt(chave_RA, plain_package)

    return_message = {
        'cyphertext': cyphertext_final,
        'recovery_point': point_final.write_bin()
    }
    return return_message


@transaction.atomic
def sign_certificate_third_cenario(vehicle_request):
    """
    TERCEIRO MODO DE OPERAÇÃO

    Função para PCA assinar certificados.
        Recebe mensagem com:
            - 'recovery_point': ponto para decifrar o texto recebido,
            - 'cyphertext': valores cifrados do certificado.

        Após a decifração de cyphertext, obtemos a estrutura:
            - 'request_id': Código de identificação do pedido
            - 'pseud_point': Ponto que deve ser assinado,
            - 'cypher_point': Ponto que deve cifrar o pacote de retorno
            - 'la_package_1': Valor cifrado recebido da LA_1
            - 'recovery_point_la_1': Ponto para decifrar la_package_1
            - 'la_package_2': Valor cifrado recebido da LA_2
            - 'recovery_point_la_2': Ponto para decifrar la_package_2

        Decifra o valor de la_package_*, obtém plv_1 e plv_2.
        Realiza a operação XOR entre ambos os valores, obtendo
        o linkage value (lv).

        Gera um bigNumber aleatório "seed" e o multiplica
        pelo valor do ponto gerador, dando origem ao ponto C.
        O pseudônimo corresponde aos valores:
            pseudonym = pseud_point + C

        Assina o hash de:
        {
            'pseudonym_bin': binário do pseudônimo final,
            'linkage_value': valor de encadeamento
        }
        e forma o pacote:
        {
            'sign_1',
            'sign_2', # dois BigNumbers correspondentes a assinatura
            'seed'
            'linkage_value'
        }
        Cifra tudo com cypher_point e obtém o pacote:
        {
            'request_id': valor recebido do request_id
            'recovery_point': ponto para decifrar o texto enviado,
            'cyphertext': texto cifrado
        }
        Esse pacote é cifrado e enviado pra RA.

        A PCA armazena todos valores, mas não consegue rastrear o veículo.
    """
    private, public = get_own_keys()

    bin_value = vehicle_request.get('recovery_point')
    recovery_point = Point()
    recovery_point.read_bin(bin_value)

    cyphertext = vehicle_request.get('cyphertext')

    decrypted_message = CryptoProtocols.ecies_decrypt(recovery_point, cyphertext, private)
    # print("decrypted: %s" % decrypted_message)

    identification = decrypted_message.get('request_id')
    pseudonym_bin = decrypted_message.get('pseud_point')

    cypherkey = Point()
    cypherkey.read_bin(decrypted_message.get('cypher_point'))

    la_package_1 = decrypted_message.get('la_package_1')
    recovery_point_la_1_bin = decrypted_message.get('recovery_point_la_1')
    recovery_point_la_1 = Point()
    recovery_point_la_1.read_bin(recovery_point_la_1_bin)
    plv_la_1 = CryptoProtocols.ecies_decrypt(recovery_point_la_1, la_package_1, private)
    # print("plv_la_1; %s" % plv_la_1)

    la_package_2 = decrypted_message.get('la_package_2')
    recovery_point_la_2_bin = decrypted_message.get('recovery_point_la_2')
    recovery_point_la_2 = Point()
    recovery_point_la_2.read_bin(recovery_point_la_2_bin)
    plv_la_2 = CryptoProtocols.ecies_decrypt(recovery_point_la_2, la_package_2, private)
    # print("plv_la_2; %s" % plv_la_2)

    linkage_value = bytes_xor(plv_la_1, plv_la_2)
    # print("linkage_value; %s" % linkage_value)

    pseud_origin_point = Point()
    pseud_origin_point.read_bin(pseudonym_bin)

    # Obtém a ordem da curva utilizada
    mod = BigNumber()
    Point.get_curve_order(mod)

    # Calcula uma semente c aleatória
    seed = BigNumber()
    seed.rand_mod(mod)

    # Obtém C = cG
    point_C = Point()
    Point.multiply_by_generator(point_C, seed)

    final_pseudonym = Point()
    Point.add(final_pseudonym, pseud_origin_point, point_C)
    final_pseudonym_bin = final_pseudonym.write_bin()

    message_to_sign = {
        'pseudonym_bin': final_pseudonym_bin,
        'linkage_value': linkage_value
    }
    signature_1, signature_2 = CryptoProtocols.ecdsa_sign(message_to_sign, private)

    content = {
        'seed': seed.write_bin(),
        'sign_1': signature_1.write_bin(),
        'sign_2': signature_2.write_bin(),
        'linkage_value': linkage_value
    }

    # Salvar no banco de dados o certificado do veículo (comportamento malicioso)
    bd_conversion.save_vehicle_certificate_butterfly(identification, seed,
        final_pseudonym, cypherkey, pseud_origin_point, signature_1, signature_2, linkage_value,
        plv_la_1, plv_la_2)

    # bd_conversion.save_vehicle_certificate(identification, pseudonym_bin, signature_1, signature_2)
    cyphertext_vehicle, point_vehicle = CryptoProtocols.ecies_encrypt(cypherkey, content)

    plain_package = {
        'request_id': identification,
        'cyphertext': cyphertext_vehicle,
        'recovery_point': point_vehicle.write_bin()
    }
    chave_RA = bd_conversion.get_RA_public_key()
    cyphertext_final, point_final = CryptoProtocols.ecies_encrypt(chave_RA, plain_package)

    return_message = {
        'cyphertext': cyphertext_final,
        'recovery_point': point_final.write_bin()
    }
    return return_message


def get_own_keys():
    """
    Retorna a chave pública (Point) e privada (BigNumber) da PCA
    """
    private_bin, public_bin = bd_conversion.get_PCA_keypair_values()
    private = BigNumber()
    public = Point()

    if private_bin and public_bin:
        # print("TEM CHAVE!")
        private.read_bin(private_bin)
        public.read_bin(public_bin)
    else:
        # ERRO
        # print("Criando chaves!")
        CryptoProtocols.generate_keys(private, public)
        bd_conversion.save_own_keys(private, public)

    return private, public


def bytes_xor(x, y):
    return [a ^ b for a, b in zip(x, y)]
