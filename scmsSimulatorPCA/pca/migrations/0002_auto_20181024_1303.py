# Generated by Django 2.0.7 on 2018-10-24 13:03

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import pca.models


class Migration(migrations.Migration):

    dependencies = [
        ('pca', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='certificate',
            name='key_pair',
        ),
        migrations.AddField(
            model_name='certificate',
            name='creation_date',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='certificate',
            name='expiration_date',
            field=models.DateField(default=pca.models.get_deadline),
        ),
        migrations.AddField(
            model_name='certificate',
            name='public_key',
            field=models.ForeignKey(default=32, on_delete=django.db.models.deletion.CASCADE, to='pca.PublicKey'),
            preserve_default=False,
        ),
    ]
