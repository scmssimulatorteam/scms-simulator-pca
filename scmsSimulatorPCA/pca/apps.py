from django.apps import AppConfig


class PcaConfig(AppConfig):
    name = 'pca'

    def ready(self):
        from pyrelic import relic_config
        relic_config.setup_relic()
