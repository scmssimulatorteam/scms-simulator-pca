from .models import PublicKey
from .models import PrivateKey
from .models import KeyPair
from .models import OwnerPublicKey
from .models import Certificate
from .models import VehicleCertificate
from .models import VehicleCertificateSeeded
from .models import VehicleCertificateButterfly
from pyrelic.relic_wrapper import BigNumber
from pyrelic.relic_wrapper import Point


def clean_string(list_of_elements):
    return str(list_of_elements).replace(" ", "")


def save_own_keys(private_key: BigNumber, public_key: Point):
    pk_bin_value = clean_string(private_key.write_bin())
    cert_pk = PrivateKey.objects.create(bin_value=pk_bin_value)

    pu_bin_value = clean_string(public_key.write_bin())
    cert_pu = PublicKey.objects.create(bin_value=pu_bin_value)

    OwnerPublicKey.objects.create(belong_to='P', public_key=cert_pu)
    keypair = KeyPair.objects.create(private_key=cert_pk, public_key=cert_pu)

    return keypair


def get_RA_public_key():
    owner_ra = OwnerPublicKey.objects.filter(belong_to='R')
    if not owner_ra:
        return None

    chave_publ_RA = Point()

    public_list = owner_ra[0].public_key.bin_value.strip('[]').split(",")
    public_bin = [int(i) for i in public_list]

    chave_publ_RA.read_bin(public_bin)
    return chave_publ_RA


def get_PCA_keypair_values():
    has_key = OwnerPublicKey.objects.filter(belong_to='P')
    if(not has_key):
        return None, None
    else:
        keypair = KeyPair.objects.filter(public_key=has_key[0].public_key)[0]

        public_list = keypair.public_key.bin_value.strip('[]').split(",")
        public_bin = [int(i) for i in public_list]

        private_list = keypair.private_key.bin_value.strip('[]').split(",")
        private_bin = [int(i) for i in private_list]

        return private_bin, public_bin


def save_vehicle_certificate(identification, pseudonym_bin, sign_1: BigNumber, sign_2: BigNumber):
    pu_bin_value = clean_string(pseudonym_bin)
    pu_key = PublicKey.objects.create(bin_value=pu_bin_value)
    OwnerPublicKey.objects.create(belong_to='V', public_key=pu_key)

    sign_1_bin = sign_1.write_bin()
    sign_2_bin = sign_2.write_bin()
    certificate = Certificate.objects.create(public_key=pu_key,
                                             signature_value_1=sign_1_bin,
                                             signature_value_2=sign_2_bin)

    identification_bin = clean_string(identification)
    vehicle_cert = VehicleCertificate.objects.create(identification_code=identification_bin,
                                                     certificate=certificate)
    return vehicle_cert


def save_vehicle_certificate_with_seed(request_id, seed: BigNumber, pseudonym: Point,
                                       cypher_point: Point, pseud_point: Point,
                                       sign_1: BigNumber, sign_2: BigNumber):
    pu_bin_value = clean_string(pseudonym.write_bin())

    pu_key = PublicKey.objects.create(bin_value=pu_bin_value)
    OwnerPublicKey.objects.create(belong_to='V', public_key=pu_key)

    seed_value = clean_string(seed.write_bin())

    sign_1_bin = sign_1.write_bin()
    sign_2_bin = sign_2.write_bin()
    certificate = Certificate.objects.create(public_key=pu_key,
                                             signature_value_1=sign_1_bin,
                                             signature_value_2=sign_2_bin)

    pseud_point_value = clean_string(pseud_point.write_bin())
    cypher_point_value = clean_string(cypher_point.write_bin())

    request_id_bin = clean_string(request_id)
    vehicle_cert = VehicleCertificateSeeded.objects.create(request_id=request_id_bin,
                                                     certificate=certificate,
                                                     seed=seed_value,
                                                     pseud_point=pseud_point_value,
                                                     cypher_point=cypher_point_value)
    return vehicle_cert


def save_vehicle_certificate_butterfly(request_id, seed: BigNumber, pseudonym: Point,
                                       cypher_point: Point, pseud_point: Point,
                                       sign_1: BigNumber, sign_2: BigNumber, linkage_value,
                                       plv_la_1, plv_la_2):
    pu_bin_value = clean_string(pseudonym.write_bin())

    pu_key = PublicKey.objects.create(bin_value=pu_bin_value)
    OwnerPublicKey.objects.create(belong_to='V', public_key=pu_key)

    seed_value = clean_string(seed.write_bin())

    sign_1_bin = sign_1.write_bin()
    sign_2_bin = sign_2.write_bin()
    certificate = Certificate.objects.create(public_key=pu_key,
                                             signature_value_1=sign_1_bin,
                                             signature_value_2=sign_2_bin)

    pseud_point_value = clean_string(pseud_point.write_bin())
    cypher_point_value = clean_string(cypher_point.write_bin())
    linkage_value_value = clean_string(linkage_value)
    plv_1 = clean_string(plv_la_1)
    plv_2 = clean_string(plv_la_2)

    request_id_bin = clean_string(request_id)
    vehicle_cert = VehicleCertificateButterfly.objects.create(request_id=request_id_bin,
                                                     certificate=certificate,
                                                     seed=seed_value,
                                                     pseud_point=pseud_point_value,
                                                     cypher_point=cypher_point_value,
                                                     linkage_value=linkage_value_value,
                                                     pre_linkage_value_1=plv_1,
                                                     pre_linkage_value_2=plv_2)
    return vehicle_cert
