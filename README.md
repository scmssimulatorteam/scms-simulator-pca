# Simulador SCMS PCA

### Pré-requisitos
- Python 3.7.0
- pip 10.0.1
- virtualenv 16.0.0
- PosgreSQL 10.4


## Configuração do banco de dados

- Criar uma database:
```
CREATE DATABASE scmssimulatorpca;   
CREATE DATABASE scmssimulatorpca_second;
CREATE DATABASE scmssimulatorpca_third;
```

- Criar o usuário e alterar suas configurações
```                                                                                          
create user scmsuser with password 'scmsprojectpassword';
ALTER ROLE scmsuser SET client_encoding TO 'utf8';
ALTER ROLE scmsuser SET default_transaction_isolation TO 'read committed';
GRANT ALL PRIVILEGES ON DATABASE scmsSimulatorPca TO scmsuser;
GRANT ALL PRIVILEGES ON DATABASE scmsSimulatorPca_second TO scmsuser;
GRANT ALL PRIVILEGES ON DATABASE scmsSimulatorPca_third TO scmsuser;
ALTER ROLE scmsuser SET timezone TO 'UTC';
```

## Instalação

- Criar um ambiente virtual
```
virtualenv env --python=python3.6 
```

- Acessar o ambiente virtual
```
source env/bin/activate
```

- Compilar a biblioteca e instalar requisitos através do Makefile
```
make all
```

- Acessar a pasta do projeto que deseja rodar. Exemplo: scmsSimulatorPCA.
```
cd scmsSimulatorPCA
```

- Aplicar migrações nos bancos de dados:
```
python manage.py migrate --settings=scmsSimulatorPCA.settings.first
python manage.py migrate --settings=scmsSimulatorPCA.settings.second
python manage.py migrate --settings=scmsSimulatorPCA.settings.third
```

- Executar scripts no banco de dados:
```
\connect scmssimulatorpca
-- Adiciona propria chave
INSERT INTO public.pca_publickey(bin_value)
    VALUES ('[2,26,103,209,130,133,76,132,35,153,82,179,29,109,186,250,201,1,111,53,214,77,183,209,171,154,152,91,143,183,242,109,166]');

INSERT INTO public.pca_privatekey(bin_value)
	VALUES ('[1,141,178,229,236,151,133,136,235,134,68,26,37,151,139,243,225,233,27,61,119,236,114,41,69,103,252,87,79,182,228,169]');

INSERT INTO public.pca_ownerpublickey(
            belong_to, public_key_id)
    VALUES ('P', (SELECT id from public.pca_publickey WHERE bin_value='[2,26,103,209,130,133,76,132,35,153,82,179,29,109,186,250,201,1,111,53,214,77,183,209,171,154,152,91,143,183,242,109,166]'));

INSERT INTO public.pca_keypair(private_key_id, public_key_id)
	VALUES (
		(SELECT id from public.pca_privatekey WHERE bin_value='[1,141,178,229,236,151,133,136,235,134,68,26,37,151,139,243,225,233,27,61,119,236,114,41,69,103,252,87,79,182,228,169]'),
		(SELECT id from public.pca_publickey WHERE bin_value='[2,26,103,209,130,133,76,132,35,153,82,179,29,109,186,250,201,1,111,53,214,77,183,209,171,154,152,91,143,183,242,109,166]')
		);

\connect scmssimulatorpca_second
-- Adiciona propria chave
INSERT INTO public.pca_publickey(bin_value)
    VALUES ('[2,26,103,209,130,133,76,132,35,153,82,179,29,109,186,250,201,1,111,53,214,77,183,209,171,154,152,91,143,183,242,109,166]');

INSERT INTO public.pca_privatekey(bin_value)
	VALUES ('[1,141,178,229,236,151,133,136,235,134,68,26,37,151,139,243,225,233,27,61,119,236,114,41,69,103,252,87,79,182,228,169]');

INSERT INTO public.pca_ownerpublickey(
            belong_to, public_key_id)
    VALUES ('P', (SELECT id from public.pca_publickey WHERE bin_value='[2,26,103,209,130,133,76,132,35,153,82,179,29,109,186,250,201,1,111,53,214,77,183,209,171,154,152,91,143,183,242,109,166]'));

INSERT INTO public.pca_keypair(private_key_id, public_key_id)
	VALUES (
		(SELECT id from public.pca_privatekey WHERE bin_value='[1,141,178,229,236,151,133,136,235,134,68,26,37,151,139,243,225,233,27,61,119,236,114,41,69,103,252,87,79,182,228,169]'),
		(SELECT id from public.pca_publickey WHERE bin_value='[2,26,103,209,130,133,76,132,35,153,82,179,29,109,186,250,201,1,111,53,214,77,183,209,171,154,152,91,143,183,242,109,166]')
		);

-- Adiciona a chave da RA
INSERT INTO public.pca_publickey(bin_value)
    VALUES ('[3,0,55,134,248,105,18,50,10,6,179,130,226,179,35,235,138,23,2,231,228,203,56,63,164,54,174,127,108,68,139,31,123]');

INSERT INTO public.pca_ownerpublickey(
            belong_to, public_key_id)
    VALUES ('R', (SELECT id from public.pca_publickey WHERE bin_value='[3,0,55,134,248,105,18,50,10,6,179,130,226,179,35,235,138,23,2,231,228,203,56,63,164,54,174,127,108,68,139,31,123]' LIMIT 1));


\connect scmssimulatorpca_third
-- Adiciona propria chave
INSERT INTO public.pca_publickey(bin_value)
    VALUES ('[2,26,103,209,130,133,76,132,35,153,82,179,29,109,186,250,201,1,111,53,214,77,183,209,171,154,152,91,143,183,242,109,166]');

INSERT INTO public.pca_privatekey(bin_value)
	VALUES ('[1,141,178,229,236,151,133,136,235,134,68,26,37,151,139,243,225,233,27,61,119,236,114,41,69,103,252,87,79,182,228,169]');

INSERT INTO public.pca_ownerpublickey(
            belong_to, public_key_id)
    VALUES ('P', (SELECT id from public.pca_publickey WHERE bin_value='[2,26,103,209,130,133,76,132,35,153,82,179,29,109,186,250,201,1,111,53,214,77,183,209,171,154,152,91,143,183,242,109,166]'));

INSERT INTO public.pca_keypair(private_key_id, public_key_id)
	VALUES (
		(SELECT id from public.pca_privatekey WHERE bin_value='[1,141,178,229,236,151,133,136,235,134,68,26,37,151,139,243,225,233,27,61,119,236,114,41,69,103,252,87,79,182,228,169]'),
		(SELECT id from public.pca_publickey WHERE bin_value='[2,26,103,209,130,133,76,132,35,153,82,179,29,109,186,250,201,1,111,53,214,77,183,209,171,154,152,91,143,183,242,109,166]')
		);

-- Adiciona a chave da RA
INSERT INTO public.pca_publickey(bin_value)
    VALUES ('[3,0,55,134,248,105,18,50,10,6,179,130,226,179,35,235,138,23,2,231,228,203,56,63,164,54,174,127,108,68,139,31,123]');

INSERT INTO public.pca_ownerpublickey(
            belong_to, public_key_id)
    VALUES ('R', (SELECT id from public.pca_publickey WHERE bin_value='[3,0,55,134,248,105,18,50,10,6,179,130,226,179,35,235,138,23,2,231,228,203,56,63,164,54,174,127,108,68,139,31,123]' LIMIT 1));
```

- Executar o programa
```
python manage.py runserver 8000 --settings=scmsSimulatorPCA.settings.first
```


## Observações

- Se for necessário compilar apenas a biblioteca novamente, basta executar o comando
```
make relic
```

- Login admin
```
user: giuliana
senha: scmsadmin
```